# Setup Layout &  Custom Server for Clean URL

Here we have a simple apps that will display list of starwars characters, and each list item clicked will redirecting us to the character detail page.

But, we also have some problem that you have to fix. First, everytime we go to person detail page, the navigation bar at the top of the webpage is disapearing. I want you to fix that, make the navvigation bar always appear at every pages.

The second problem is, the link for person detail page isn't so beautiful and it's not a SEO friendly link. The link appears like this `/person?id=1`. You have to fix the link, and make it prettier and more SEO friendly like `/person/1`. To achieve that, you can setup a custom server for Next.js.